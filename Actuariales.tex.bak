\documentclass[xcolor=dvipsnames,14pt,spanish]{beamer}
%\usepackage{default}

\usepackage[spanish]{babel}	%%Tradicir t�tulos al espa�ol
\usepackage[latin1]{inputenc}	%%Usar acentos y signos de interrogaci�n
\usepackage{mathptmx}		%%Se recomienda sobre pslatex
\usepackage{amsmath}		%%Componer f�rmulas de gran complejidad
\usepackage{graphicx}		%%Usar gr�ficos
%\usepackage{theorems}		%%Observaciones, definiciones, teoremas
\usepackage{url}		%%Usar \url{}
\usepackage{hyperref}		%%Marca los enlaces
\usepackage{symbols}		%%S�mbolos |RR \NN (DIRECTORIO)

\usepackage{color}              %%Para color
\usepackage{colortbl}           %%Colorear tablas, incluye paquetes array y color

\usepackage{pstricks}		%% Paquete ps-trcks
\usepackage{pst-all}
\usepackage{pst-math}		%%EXP GAUSS SIN COS TAN

\decimalpoint

\usepackage{parskip}


%%Genera error en beamer
\usepackage{harvard}


%%Usar de temas Berkeley, Madrid, Hannover, classic, default, plain, Bergen, Marburg
%%/usr/share/texmf/tex/latex/beamer/beamertheme
%% AnnArbor, Antibes, Bergen, Berkeley, Berlin, Boadilla, boxes, CambridgeUS, Copenhagen, Darmstadt, Dresden, Frankfurt, Goettingen, Hannover, Ilmenau, JuanLesPins, Luebeck, Madrid, Malmoe, Marburg, Montpellier, PaloAlto, Pittsburgh, Rochester, Singapore, Szeged, Warsaw, bars, boxes, classic, compatibility, default, lined, plain, shadow, sidebar, split, tree. 


%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
\usetheme{Berlin}
%\usetheme{Boadilla}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Dresden}
%\usetheme{Frankfurt}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{Madrid}
%\usetheme{Marburg}
%\usetheme{Rochester}
%\usetheme{bars}
%\usetheme{classic}
%%\usetheme{default}
%\usetheme{shadow}
%\usetheme{split}
%\usetheme{tree}


%%Usar color en el tema
%%/usr/share/texmf/tex/latex/beamer/
% albatross, beaver, beetle, crane, default, dolphin, dove, fly, lily, orchid, rose, seagull, %seahorse, sidebartab, structure, whale, wolverine

%\usecolortheme{beaver}
\usecolortheme[named=black]{structure}
%\usecolortheme{beetle}
%\usecolortheme{crane}
%\usecolortheme{dolphin}
%\usecolortheme{fly}
%\usecolortheme{lily}
%\usecolortheme{orchid}
%%\usecolortheme{seagull}
%%\usecolortheme{sidebartab}
%%\usecolortheme{wolverine}


%%Usar tipograf�a
%%\usefonttheme{} professionalfonts,structurebold,default,serif,structureitalicserif
%%\usefonttheme{professionalfonts}

\usefonttheme{serif}


%Anexar efectos de transici�n
%Se coloca en el FRAME (DIAPOSITIVA) SIGUIENTE
%\transblindshorizontal[duration=1]
%\transblindsvertical[duration=1]
%\transboxin[duration=1]
%\transboxout[duration=1]
%\transdissolve[duration=1]
%\transglitter[duration=1]
%\transsplitverticalin[duration=1]
%\transsplitverticalout[duration=1]
%\transsplithorizontalin[duration=1]
%\transsplithorizontalout[duration=1]
%\transwipe[duration=1,direction=180]


%%Modificar el t�tulo de las gr�ficas (figuras).
%\renewcommand{\figurename}{Gr�fica}
%\renewcommand{\listfigurename}{�ndice de gr�ficas}

%%Modificar el t�tulo de las gr�ficas (cuadros).
%\renewcommand{\tablename}{Tabla}
%\renewcommand{\listtablename}{�ndice de tablas}

%\DeclareMathOperator{\sign}{sgn}
\DeclareMathOperator{\dif}{d}
%\DeclareMathOperator*{\maxi}{maximizar}
%\DeclareMathOperator*{\conv}{\ast}

\usepackage{ragged2e}
\justifying

\title[Las matem�ticas actuariales del seguro de vida aplicadas a la seguridad social: las pensiones privadas en M�xico]
{Las matem�ticas actuariales del seguro de vida aplicadas a la seguridad social:\\[0.3cm] las pensiones privadas en M�xico}
%\titlegraphic{\includegraphics[height=1.2cm]{../../UAMI}\hfill\includegraphics[height=1.2cm]{../../UAMI}}
\subtitle{\textcolor{red}{\footnotesize Pensiones Privadas en M�xico}}
\author{Dr.~ Jos� Antonio Climent Hern�ndez}
\institute{\textcolor{red}{\url{antoniocliment@ciencias.unam.mx}}}
\date{\footnotesize 21 de mayo de~ 2015}

\begin{document}

\renewcommand{\figurename}{Gr�fica}
\renewcommand{\tablename}{Cuadro}

\bibliographystyle{dcu}
\renewcommand{\harvardand}{y}
\citationmode{default}


%Funcion R en R.
%Argumento opcional para la funcion.
%Primer argumento es la variable
\newcommand{\f}[2][f]{#1\left(#2\right)}


\theoremstyle{definition}
\newtheorem{teorema}{Teorema}
\newcommand{\teoremaname}{Teorema}
\newtheorem{proposicion}{Proposici�n}
\newcommand{\proposicionname}{Proposici�n}
\newtheorem{definicion}{Definici�n}
\newcommand{\definicionname}{Definici�n}
\newtheorem{propiedad}{Propiedad}
\newcommand{\propiedadname}{Propiedad}
\newtheorem{nota}{Nota}
\newcommand{\notaname}{Nota}


\begin{frame}
\transdissolve[duration=0.1,direction=360]

\begin{center}
\begin{tabular}{m{1.1cm}>{\centering}m{7.4cm}m{1.1cm}}
\includegraphics[height=1.1cm]{UAEM} &
{\fontsize{12pt}{14.4pt}\scshape Las Matem�ticas Actuariales del Seguro de Vida Aplicadas\linebreak a la Seguridad Social} &
\includegraphics[height=1.1cm]{UAMI} \\
\end{tabular}
\hrule\hrule\vspace{1pt}\hrule

\vspace*{0.3cm}
{\scshape\small Las Pensiones Privadas en M�xico}
\vspace*{0.1cm}

{\small\scshape Dr.~ Jos� Antonio Climent Hern�ndez}\\[0.15cm]
{\footnotesize\scshape Universidad Aut�noma Metropolitana Azcapotzalco}\\[-0.3cm]
{\scriptsize\scshape Divisi�n de Ciencias Sociales y Humanidades}\\[-0.3cm]
{\scriptsize\scshape Departamento de Administraci�n}\\
{\scriptsize\url{antoniocliment@ciencias.unam.mx}}
\end{center}
\end{frame}

%\parindent 0.8em

\include{Resumen}
\include{Introduccion}
\include{Objetivos}


\include{Bibliografia}




\end{document}
